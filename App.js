import 'react-native-gesture-handler';
import { Text, View } from 'react-native'
import React, { Component } from 'react'
import Tabbar from './src/navigation/Tabbar';
import { NavigationContainer } from '@react-navigation/native';

export default class App extends Component {
    render() {
        return (
            <NavigationContainer>
                <Tabbar/>
            </NavigationContainer>
        )
    }
}