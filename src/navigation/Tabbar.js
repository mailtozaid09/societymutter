import React,{useEffect, useState} from 'react'
import { Text, View, Image, LogBox,  } from 'react-native'

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import HomeScreen from '../screens/HomeScreen';
import ExploreScreen from '../screens/ExploreScreen';
import ProfileScreen from '../screens/ProfileScreen';

import Colors from '../styles/Colors';

const Tab = createBottomTabNavigator();

export default function Tabbar() {

    LogBox.ignoreAllLogs(true)
    
    useEffect(async() => {
        //console.log("Tabbar");
    }, []);

    return (
        <Tab.Navigator
            initialRouteName='Explore'
            screenOptions={({ route }) => ({
                tabBarStyle: {height: 60, },
                tabBarIcon: ({ focused, color, size }) => {
                let iconName;
    
                if (route.name === 'Home') {
                    iconName = focused
                    ? require('../../assets/activeHome.png')
                    : require('../../assets/Home.png')
                } else if (route.name === 'Explore') {
                    iconName = focused
                    ? require('../../assets/activeExplore.png')
                    : require('../../assets/Explore.png')
                } else if (route.name === 'Profile') {
                    iconName = focused
                    ? require('../../assets/activeProfile.png')
                    : require('../../assets/Profile.png')
                } 
                return(
                    <View style={{alignItems: 'center'}} >
                        <Image source={iconName} style={{height: 28, width: 28, resizeMode: 'contain'}}/>
                        <Text style={{fontSize: 12, color: focused ? Colors.PURPLE : Colors.GRAY}} >{route.name}</Text>
                    </View>
                );
                },
                tabBarActiveTintColor: 'tomato',
                tabBarInactiveTintColor: 'gray',
            })}
        >
            <Tab.Screen
                name="Home"
                component={HomeScreen}
                options={{
                    title: 'Home',
                    headerTitleAlign: 'center',
                    tabBarShowLabel: false
                }}
            />
            <Tab.Screen
                name="Explore"
                component={ExploreScreen}
                options={{
                    title: 'Explore',
                    headerTitleAlign: 'center',
                    tabBarShowLabel: false
                }}
            />
             <Tab.Screen
                name="Profile"
                component={ProfileScreen}
                options={{
                    title: 'Profile',
                    headerTitleAlign: 'center',
                    tabBarShowLabel: false
                }}
            />
        </Tab.Navigator>
    );
}