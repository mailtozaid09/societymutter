import React,{ useState, useEffect } from 'react'
import { Text, View, StyleSheet, ScrollView, TouchableOpacity, Image, Dimensions,  } from 'react-native'
import Button from '../components/button/Button';
import Input from '../components/input/Input';
import Colors from '../styles/Colors';

const {width} = Dimensions.get('window');

const ExploreScreen = ({navigation}) => {

    const [textValue, setTextValue] = useState('');
    const [error, setError] = useState('');

    const isTextValue = () => {

        if(textValue){
            navigation.navigate('Home', {text: textValue})
        }else{
            setError('Enter the Text!')
        }
        
    }
    return (
        <View style={styles.container} >
            <View style={styles.padding} >
                <Input
                    value={textValue}
                    onChangeText={(text) => {setTextValue(text); setError('')}}
                    placeholder="Enter the text"
                />

                <View style={styles.button} >
                    {error
                    ?
                    <Text style={styles.errorStyle} >{error}</Text>
                    :
                    null
                    }
                    <Button
                        title="Go To Home Screen"
                        onPress={() => {
                            isTextValue()
                        }}
                    />
                </View>
            </View>
           
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    button: {
        paddingTop: 50,
        alignItems: 'center',
    },
    padding: {
        width: width-50,
    },
    errorStyle: {
        fontSize: 20,
        lineHeight: 22,
        color: Colors.RED,
        marginBottom: 10,
    }
})

export default ExploreScreen