import React,{ useState, useEffect } from 'react'
import { Text, View, StyleSheet, ScrollView, TouchableOpacity, Image, Dimensions,  } from 'react-native'
import Button from '../components/button/Button';
import { useNavigation } from '@react-navigation/native';
import Colors from '../styles/Colors';

const {width} = Dimensions.get('window');

const HomeScreen = (props) => {

    const navigation = useNavigation();

    const [textValue, setTextValue] = useState(props.route.params.text);

    useEffect(() => {
        console.log("Home Screen");
        //console.log(props.route.params.text);
    }, [])
    

    return (
        <View style={styles.container} >
            <View style={styles.padding} >
                
                <View style={styles.textContainer}>
                    <Text style={styles.textStyle} >{textValue}</Text>
                </View>

                <View style={styles.button} >
                    <Button
                        title="Go To Profile Screen"
                        onPress={() => navigation.navigate('Profile')}
                    />
                </View>
            </View>
           
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    button: {
        paddingTop: 50
    },
    padding: {
        width: width-50,
    },
    textContainer: {
        height: 70,
        width: '100%',
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors.WHITE,
    },
    textStyle: {
        fontSize: 18,
        lineHeight: 24,
        color: Colors.BLACK
    }
})

export default HomeScreen