import React,{ useState, useEffect } from 'react'
import { Text, View, StyleSheet, ScrollView, TouchableOpacity, Image, Dimensions,  } from 'react-native'
import Colors from '../styles/Colors';

const {width} = Dimensions.get('window');

const ProfileScreen = () => {
    return (
        <View style={styles.container} >
            <Text style={styles.textStyle}>This is Profile Page</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    textStyle: {
        fontSize: 22,
        lineHeight: 24,
        color: Colors.BLACK
    }
})

export default ProfileScreen