import React, { Component } from 'react'
import { Text, View, StyleSheet, TouchableOpacity, Dimensions,  } from 'react-native'
import Colors from '../../styles/Colors';


const {width} = Dimensions.get('window');

const Button = (props) => {

    const { title, onPress } = props;

    return (
        <TouchableOpacity style={styles.buttonStyle} onPress={onPress} >
            <Text style={styles.textStyle} >{title}</Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    buttonStyle: {
        height: 50,
        width: '100%',
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors.PURPLE
    },
    textStyle: {
        fontSize: 18,
        lineHeight: 24,
        color: Colors.WHITE
    }
})

export default Button