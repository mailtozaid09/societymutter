import React, { Component } from 'react'
import { Text, View, StyleSheet, TextInput, Dimensions,  } from 'react-native'
import Colors from '../../styles/Colors';


const {width} = Dimensions.get('window');

const Input = (props) => {

    const { value, onChangeText, placeholder } = props;

    return (
        <View>
            <TextInput
                value={value}
                placeholder={placeholder}
                onChangeText={onChangeText}
                style={styles.textInput}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    textInput: {
        borderRadius: 10,
        width: '100%',
        paddingLeft: 20,
        borderWidth: 1,
        backgroundColor: Colors.WHITE,
    }
})

export default Input